import Vue from 'vue'
import VueResource from 'vue-resource'
import App from './App'

/*import '../semantic/dist/semantic.min.css'
import '../semantic/dist/semantic.min.js'
*/
Vue.use(VueResource)
Vue.http.options.root = 'http://jsonplaceholder.typicode.com'
Vue.http.headers.common['Authorization'] = 'Basic YXBpOnBhc3N3b3Jk';

Vue.http.interceptors.push((request, next) => {
  next((response) => {
    if (request.after) {
      request.after.call(this, response)
    }
  })
})
new Vue({
  el: '#app',
  render: h=> h(App)
})
